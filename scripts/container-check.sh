#!bin/bash
docker ps | grep cicd-docker
if [echo $? == 0]
then 
echo "finding docker container id"
app_container=`docker ps | grep cicd-docker | awk '{print $1}'`
lasttime=`date +"%d.%m.%Y-%H.%M"`
docker rename cicd-docker cicd-docker-$lasttime
docker stop $app_container && docker run -d --name cicd-docker 760267871267.dkr.ecr.us-east-2.amazonaws.com/cicd-docker
else
docker run -d --name cicd-docker 760267871267.dkr.ecr.us-east-2.amazonaws.com/cicd-docker
fi