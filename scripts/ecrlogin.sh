#!bin/bash

aws ecr get-login-password \
        --region us-east-2  \
| docker login --username AWS \
        --password-stdin 760267871267.dkr.ecr.us-east-2.amazonaws.com/cicd-docker
echo "pulling image from ECR"
docker pull 760267871267.dkr.ecr.us-east-2.amazonaws.com/cicd-docker:latest